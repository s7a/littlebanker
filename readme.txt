App => LittleBanker

Run => In order to start the application use the Maven cmd "mvn spring-boot:run" this will fire up the spring-boot embedded Tomcat on port 8080 on you localhost

Test => The easiest way to test the functionality is to import the SOAPUI project within this repro.
		In case you are unfamiliar with SOAPUI, the application defines the following endpoints:
		
		1) /accounts
			Method: get
			Method: post => e.g. {"balance":100	}
			
			/accounts/{id}
			Method:get
			
			
		2) /payments
			Method: post => e.g. "from":1, "to":2, "amount":50
			
		   /payments?id={id}&fromDate={fromDate}&toDate={toDate}
		   Method: get
			

