package io.arturo.littlebanker.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.arturo.littlebanker.model.transaction.Transaction;
import io.arturo.littlebanker.service.payment.IPaymentService;

@RestController
@CrossOrigin
public class PaymentController {

	@Autowired
	IPaymentService payService;

	@PostMapping(value = "/payments")
	public ResponseEntity<Transaction> processPayment(@RequestBody Transaction transaction) {

		transaction.setTransDate(new Date());
		final Transaction completedTransaction = payService.processPayment(transaction);
		if (completedTransaction == null) {
			return new ResponseEntity<Transaction>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<Transaction>(completedTransaction, HttpStatus.ACCEPTED);

	}

	/**
	 * @param id
	 * @param fromDateStr
	 *            => format "MM/DD/yyyy"
	 * @param toDateStr
	 *            => format "MM/DD/yyyy"
	 * @return
	 * 
	 */
	@GetMapping(value = "/payments")
	public ResponseEntity<List<Transaction>> findPaymentsByAccIdFilteredByDate(@RequestParam("id") Long id,
			@RequestParam("fromDate") String fromDateStr, @RequestParam("toDate") String toDateStr) {

		final DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
		Date fromDate = null;
		Date toDate = null;
		try {
			fromDate = df.parse(fromDateStr);
			toDate = df.parse(toDateStr);
		} catch (final ParseException e) {
			return new ResponseEntity<List<Transaction>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<Transaction>>(payService.findPaymentsByAccIdFilteredByDate(id, fromDate, toDate),
				HttpStatus.ACCEPTED);
	}

}
