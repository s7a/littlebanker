package io.arturo.littlebanker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.arturo.littlebanker.model.account.Account;
import io.arturo.littlebanker.service.account.IAccountService;

@RestController
@CrossOrigin
public class AccountController {

	@Autowired
	IAccountService accountService;

	@PostMapping(value = "/accounts")
	public Account createAccount(@RequestBody Account account) {

		return accountService.createAccount(account);
	}

	@GetMapping(value = "/accounts/{id}")
	public Account getAccountById(@PathVariable("id") Long id) {

		return accountService.getAccountById(id);
	}

	@GetMapping(value = "/accounts")
	public List<Account> getAllAccounts() {

		return accountService.getAllAccounts();
	}

}
