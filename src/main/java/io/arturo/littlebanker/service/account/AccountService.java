package io.arturo.littlebanker.service.account;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.arturo.littlebanker.model.account.Account;
import io.arturo.littlebanker.model.account.AccountRepository;

@Service
public class AccountService implements IAccountService {

	@Autowired
	AccountRepository repro;

	public Account createAccount(Account account) {

		return repro.save(account);
	}

	public List<Account> getAllAccounts() {

		return repro.findAll();
	}

	public Account getAccountById(Long id) {

		return repro.findById(id);
	}

}
