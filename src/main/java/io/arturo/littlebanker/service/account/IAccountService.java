package io.arturo.littlebanker.service.account;

import java.util.List;

import io.arturo.littlebanker.model.account.Account;

public interface IAccountService {

	Account createAccount(Account account);

	Account getAccountById(Long id);

	List<Account> getAllAccounts();

}
