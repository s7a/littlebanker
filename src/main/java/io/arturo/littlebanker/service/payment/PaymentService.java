package io.arturo.littlebanker.service.payment;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.arturo.littlebanker.model.account.Account;
import io.arturo.littlebanker.model.account.AccountRepository;
import io.arturo.littlebanker.model.transaction.Transaction;
import io.arturo.littlebanker.model.transaction.TransactionRepository;

@Service
public class PaymentService implements IPaymentService {

	@Autowired
	AccountRepository accountRepro;
	@Autowired
	TransactionRepository transRepro;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.arturo.littlebanker.service.payment.IPaymentService#processPayment(io.
	 * arturo.littlebanker.model.transaction.Transaction) The only method which
	 * requires a comment => If the client passes in invalid account id's or the
	 * specified amount is negative we need a way to react on this in our
	 * controller
	 */
	@Transactional
	public Transaction processPayment(Transaction transaction) {

		final Account fromAccount = accountRepro.findById(transaction.getFrom());
		final Account toAccount = accountRepro.findById(transaction.getTo());

		if (fromAccount == null || toAccount == null || transaction.getAmount().signum() == -1) {
			return null;
		}

		fromAccount.setBalance(fromAccount.getBalance().subtract(transaction.getAmount()));
		toAccount.setBalance(toAccount.getBalance().add(transaction.getAmount()));
		final Transaction transactionComplete = transRepro.save(transaction);

		return transactionComplete;

	}

	public List<Transaction> findPaymentsByAccIdFilteredByDate(Long accId, Date fromDate, Date toDate) {

		return transRepro.findByAccIdFilteredByDate(accId, fromDate, toDate);
	}

}
