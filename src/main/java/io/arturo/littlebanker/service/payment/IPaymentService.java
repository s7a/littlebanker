package io.arturo.littlebanker.service.payment;

import java.util.Date;
import java.util.List;

import io.arturo.littlebanker.model.transaction.Transaction;

public interface IPaymentService {

	Transaction processPayment(Transaction transaction);

	List<Transaction> findPaymentsByAccIdFilteredByDate(Long accId, Date fromDate, Date toDate);

}
