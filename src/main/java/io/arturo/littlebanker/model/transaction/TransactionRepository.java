package io.arturo.littlebanker.model.transaction;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	@Query("select t from Transaction t where (t.fromAcc = ?1 OR t.toAcc = ?1)  AND (t.transDate >=?2 AND t.transDate <=?3)")
	List<Transaction> findByAccIdFilteredByDate(Long accId, Date fromDate, Date toDate);

}
