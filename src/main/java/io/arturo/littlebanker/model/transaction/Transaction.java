package io.arturo.littlebanker.model.transaction;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private Long fromAcc;

	private Long toAcc;

	private BigDecimal amount;

	private Date transDate;

	public Transaction() {
	};

	public Transaction(Long from, Long to, BigDecimal amount) {
		this.fromAcc = from;
		this.toAcc = to;
		this.amount = amount;
		this.transDate = new Date();
	}

	public Long getFrom() {
		return fromAcc;
	}

	public void setFrom(Long from) {
		this.fromAcc = from;
	}

	public Long getTo() {
		return toAcc;
	}

	public void setTo(Long to) {
		this.toAcc = to;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

}
